<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Login</title>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href="login.css" rel="stylesheet">
  </head>
  <body class="text-center">
    <form class="form-signin" action="" method = "post">
      <img class="mb-4" src="img/6.png" alt="" >
      <h1 class="h3 mb-3 font-weight-normal">Iniciar sesion </h1>
      <label for="inputEmail" class="sr-only">usuario</label>
      <input type="text"  id="inputEmail" name="login" class="form-control" placeholder="Login" required autofocus>
      <label for="inputPassword" class="sr-only">Password</label>
      <input type="password"  name="password" id="inputPassword" class="form-control" placeholder="Password" required>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Aceptar</button>
      <br/>
      <p class="mt-5 mb-3 text-muted">&copy; P.C.S Martha A. Chavez</p>
    </form>
  </form>

  </body>
</html>
